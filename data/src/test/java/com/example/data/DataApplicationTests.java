package com.example.data;

import com.example.data.entity.*;
import com.example.data.repository.CustomerRepository;
import com.example.data.repository.ProductRepository;
import com.example.data.repository.ProgrammerRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;


@SpringBootTest
class DataApplicationTests {
    @Autowired
    ProductRepository productRepository;
    @Autowired
    CustomerRepository customerRepository;
    @Autowired
    ProgrammerRepository programmerRepository;

    @Test
    void contextLoads() {
    }

    @Test
    void testCreate() {
        productRepository.save(new Product(1L, "jpa", "spring data jpa course", 20d));
    }

    @Test
    void testRead() {
        Product product = productRepository.findById(1L).get();
        System.out.println(product.getName());
    }

    @Test
    void testUpdate() {
        Product product = productRepository.findById(1L).get();
        product.setPrice(200d);
        productRepository.save(product);
        System.out.println(product.getPrice());
    }

    @Test
    void testDelete() {
        productRepository.deleteById(1L);
    }

    @Test
    void testCreateCustomer() {
        Customer customer = new Customer();
        customer.setName("xd");

        PhoneNumber phoneNumber1 = new PhoneNumber();
        phoneNumber1.setNumber("1234");
        phoneNumber1.setType("cell");

        customer.addPhoneNumber(phoneNumber1);

        PhoneNumber phoneNumber2 = new PhoneNumber();
        phoneNumber2.setNumber("057340");
        phoneNumber2.setType("home");
        customer.addPhoneNumber(phoneNumber2);
        customerRepository.save(customer);
    }
    @Test
    @Transactional
    void testReadCustomer(){
       Customer customer =  customerRepository.findById(1L).get();
       customer.getPhoneNumbers().forEach(phoneNumber -> System.out.println(phoneNumber.getNumber()));
    }

    @Test
    void testUpdateCustomer(){
        Customer customer =  customerRepository.findById(1L).get();
        customer.setName("xcode");
        customerRepository.save(customer);
    }

    @Test
    void testDeleteCustomer(){
        customerRepository.deleteById(1L);
    }

    @Test
    void testCreateProgrammer(){
        Project project = new Project();
        Set<Project> projects = new HashSet<>();
        projects.add(project);
        project.setName("cs");
        Programmer programmer = new Programmer();
        programmer.setName("dev");
        programmer.setSalary(500d);
        programmer.setProjects(projects);
        programmerRepository.save(programmer);
    }

    @Test
    @Transactional
    void testReadProgrammer(){
        Programmer programmer =  programmerRepository.findById(1L).get();
        System.out.println(programmer);
        System.out.println(programmer.getProjects());
    }
}
